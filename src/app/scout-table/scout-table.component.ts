import { Component, OnInit } from '@angular/core';

interface Scout {
    name: string;
    id: number;
    complete: boolean;
}

@Component({
  selector: 'app-scout-table',
  templateUrl: './scout-table.component.html',
  styleUrls: ['./scout-table.component.css']
})
export class ScoutTableComponent implements OnInit {

  apiInfo = {

      rank: 'tender',
      rankId: 12,
      requirementId: 43,
      requirementDescription: `I'm a teapot`,
      scouts: [
          {
            name: 'sam',
            id: 12,
            complete: false
          }, {
            name: 'fred',
            id: 34,
            complete: true
          },
          {
            name: 'bob',
            id: 100,
            complete: true
          }
      ]
  };

  constructor() { }

  ngOnInit() {
  }

  toggleRequirementCompletion(scout: Scout) {

      scout.complete = !scout.complete;
  }

  /**
   * Toggles whether `htmlElement` should be printed
   */
  togglePrintDisplay(htmlElement: HTMLElement) {

      htmlElement.classList.toggle('hide-from-print');
  }

}
